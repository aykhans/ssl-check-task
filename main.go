package main

import (
	"fmt"
	"log"
	"strings"
	"sync"
	"time"

	"github.com/aykhans/ssl-check-task/ssl_checker"
)

const (
	REQUEST_TIMEOUT          = 10 * time.Second   // request timeout for each domain
	GOROUTINES_COUNT         = 3                  // goroutines for parallel requests
	EXPIRATION_THRESHOLD     = 1 * 24 * time.Hour // certificate expiration threshold
	RATE_LIMIT_REQUEST_COUNT = 4                  // requests count per RATE_LIMIT_TIME
	RATE_LIMIT_TIME          = 5 * time.Second    // RATE_LIMIT_REQUEST_COUNT requests per RATE_LIMIT_TIME
)

func main() {
	domains := []string{
		"docker.com",
		"aykhans.me",
		"apple.com",
		"samsung.com",
		"kontakt.az",
		"x.com",
		"instagram.com",
		"linkedin.com",
		"github.com",
		"youtube.com",
	}

	if GOROUTINES_COUNT > RATE_LIMIT_REQUEST_COUNT {
		log.Fatalf("GOROUTINES_COUNT should be less than domains count of each session")
	}

	domainsCount := len(domains)
	for i := 0; i < domainsCount; i += RATE_LIMIT_REQUEST_COUNT {
		fmt.Println(strings.Repeat("-", 70))
		if i+RATE_LIMIT_REQUEST_COUNT > domainsCount {
			processDomains(domains[i:], min(GOROUTINES_COUNT, domainsCount-i))
			break
		} else {
			processDomains(domains[i:i+RATE_LIMIT_REQUEST_COUNT], GOROUTINES_COUNT)
			if i+RATE_LIMIT_REQUEST_COUNT == domainsCount {
				break
			}
		}
		time.Sleep(RATE_LIMIT_TIME)
	}
}

// ProcessDomains processes the given list of domains concurrently using goroutines.
// It divides the domains into equal parts based on the number of goroutines specified.
// Each goroutine checks the expiration date of the domains assigned to it.
// The function waits for all goroutines to finish before returning.
func processDomains(domains []string, goRoutinesCount int) {
	wg := sync.WaitGroup{}
	wg.Add(goRoutinesCount)
	for i := 0; i < goRoutinesCount; i++ {
		go func(domains []string, i int) {
			defer wg.Done()
			currentTime := getCurrentTime()
			for _, domain := range domains {
				checkExpDate(domain, currentTime)
			}
		}(domains[i*len(domains)/goRoutinesCount:(i+1)*len(domains)/goRoutinesCount], i)
	}
	wg.Wait()
}

// Checks the expiration date of the SSL certificate and compares with the current time
// and sends a notification if the expiration date is within the threshold.
func checkExpDate(domain string, currentTime time.Time) {
	t, err := sslchecker.GetCertExpDate(domain, REQUEST_TIMEOUT)
	if err != nil {
		log.Println(err)
	}
	if t.Sub(currentTime) <= EXPIRATION_THRESHOLD {
		sendExpDateNotification(domain)
	}
	fmt.Printf("Domain: %s | Expiration Date: %s\n", domain, t)
}

func sendExpDateNotification(domain string) {
	log.Println("Sending notification for", domain)
}

func getCurrentTime() time.Time {
	return time.Now()
}
