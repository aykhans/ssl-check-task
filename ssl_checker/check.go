package sslchecker

import (
	"crypto/tls"
	"errors"
	"net"
	"time"
)

// Returns the expiration date of the SSL certificate for the given domain.
// It establishes a TCP connection to the domain on port 443 and retrieves the certificate.
// The function returns the expiration date as a pointer to a time.Time value and an error.
func GetCertExpDate(domain string, timeout time.Duration) (*time.Time, error) {
	conn, err := net.DialTimeout("tcp", domain+":443", timeout)
	if err != nil {
		return nil, err
	}
	defer conn.Close()

	config := tls.Config{ServerName: domain}
	tlsConn := tls.Client(conn, &config)

	if err = tlsConn.Handshake(); err != nil {
		return nil, err
	}

	certs := tlsConn.ConnectionState().PeerCertificates
	if len(certs) == 0 {
		return nil, errors.New("no certificates found")
	}
	return &certs[0].NotAfter, nil
}
